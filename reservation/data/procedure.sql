DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `add_rating_to_reservation`(IN reservation_id INT, IN evaluation DOUBLE)
BEGIN
	UPDATE reservation 
    SET reservation.rating_points = reservation.rating_points + evaluation, reservation.rating_count = reservation.rating_count + 1
    WHERE reservation.id = reservation_id;
END$$
DELIMITER ;
