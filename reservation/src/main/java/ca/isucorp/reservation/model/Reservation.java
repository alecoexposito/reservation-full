package ca.isucorp.reservation.model;

import org.hibernate.annotations.Formula;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
public class Reservation {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  @Column(length = 4000)
  @NotEmpty
  private String description;

  @ManyToOne(cascade = CascadeType.MERGE)
  @JoinColumn(name = "contact_id")
  @NotNull
  private Contact contact;
  @NotNull
  private Date reservationDate;
  private Double ratingPoints = 0.0;
  private Integer ratingCount = 0;
  @Formula(value = "coalesce(rating_points / NULLIF(rating_count, 0), 0)")
  private Double rating;

  public Reservation() {
  }

  public Double getRatingPoints() {
    return ratingPoints;
  }

  public void setRatingPoints(Double ratingPoints) {
    this.ratingPoints = ratingPoints;
  }

  public Integer getRatingCount() {
    return ratingCount;
  }

  public void setRatingCount(Integer ratingCount) {
    this.ratingCount = ratingCount;
  }

  public Contact getContact() {
    return contact;
  }

  public void setContact(Contact contact) {
    this.contact = contact;
  }

  public Date getReservationDate() {
    return reservationDate;
  }

  public void setReservationDate(Date reservationDate) {
    this.reservationDate = reservationDate;
  }

  public Reservation(String description, Contact contact, Date reservationDate) {
    this.description = description;
    this.contact = contact;
    this.reservationDate = reservationDate;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public double getRating() {
    if (ratingCount > 0)
      return ratingPoints / ratingCount;
    return 0.0;
  }
}
