package ca.isucorp.reservation.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.context.MessageSource;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Date;
import java.util.List;

@Entity
public class Contact {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  @Column(unique = true)
  @NotEmpty
  private String name;
  @NotNull
  private Date birthDate;

  @Pattern(regexp = "(\\+)*[0-9]+")
  private String phone;

  @JsonIgnore
  @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "contact")
  public List<Reservation> reservations;

  public Contact() {
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public Contact(String name, Date birthDate, ContactType contactType, String phone) {
    this.name = name;
    this.birthDate = birthDate;
    this.type = contactType;
    this.phone = phone;
  }


  @ManyToOne(cascade = CascadeType.MERGE)
  @JoinColumn(name = "type_id")
  private ContactType type;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Date getBirthDate() {
    return birthDate;
  }

  public void setBirthDate(Date birthDate) {
    this.birthDate = birthDate;
  }

  public ContactType getType() {
    return type;
  }

  public void setType(ContactType type) {
    this.type = type;
  }
}
