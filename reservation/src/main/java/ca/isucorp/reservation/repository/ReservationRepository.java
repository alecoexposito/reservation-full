package ca.isucorp.reservation.repository;

import ca.isucorp.reservation.model.Reservation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.query.Procedure;

public interface ReservationRepository extends JpaRepository<Reservation, Integer> {
  /**
   * Method that calls a stored procedure en the database to add a new rating to a reservation
   * @param id
   * @param evaluation
   * @return
   */
  @Procedure("add_rating_to_reservation")
  void addRatingToReservation(Integer id, Double evaluation);
}
