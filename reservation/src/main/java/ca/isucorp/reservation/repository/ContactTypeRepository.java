package ca.isucorp.reservation.repository;

import ca.isucorp.reservation.model.ContactType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ContactTypeRepository extends JpaRepository<ContactType, Integer> {
}
