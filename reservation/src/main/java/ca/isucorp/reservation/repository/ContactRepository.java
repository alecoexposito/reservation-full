package ca.isucorp.reservation.repository;

import ca.isucorp.reservation.model.Contact;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ContactRepository extends JpaRepository<Contact, Integer> {
  List<Contact> findByNameContaining(String term);
}
