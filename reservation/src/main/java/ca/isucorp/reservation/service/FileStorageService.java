package ca.isucorp.reservation.service;

import ca.isucorp.reservation.util.Util;
import org.apache.tomcat.util.http.fileupload.impl.IOFileUploadException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

/**
 * Service class to handle file upload.
 */
@Service
public class FileStorageService {

  private Path fileStorageLocation;

  @Value("${file.upload-dir}")
  private String uploadDir;

  @Autowired
  public FileStorageService() throws Exception {
  }

  /**
   * Creates uploads folder if it doesn't exists.
   * @throws Exception
   */
  @PostConstruct
  public void createUploadDir() throws Exception {
    this.fileStorageLocation = Paths.get(this.uploadDir)
      .toAbsolutePath().normalize();
    try {
      Files.createDirectories(this.fileStorageLocation);
    } catch (Exception ex) {
      throw new Exception("Could not create the directory where the uploaded files will be stored.", ex);
    }
  }

  /**
   * Service method for uploading a file
   *
   * @param file
   * @return
   * @throws IOFileUploadException
   */
  public String storeFile(MultipartFile file) throws IOFileUploadException {
    // Normalize file name
    String fileName = StringUtils.cleanPath(file.getOriginalFilename());

    try {
      // Check if the file's name contains invalid characters
      if (fileName.contains("..")) {
        throw new IOFileUploadException("Sorry! Filename contains invalid path sequence " + fileName, null);
      }
      String name = Util.getUniqueFileName(Util.getExtFromFilename(fileName));

      // Copy file to the target location (Replacing existing file with the same name)
      Path targetLocation = this.fileStorageLocation.resolve(name);
      Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
      return ServletUriComponentsBuilder.fromCurrentContextPath().build().toUriString() + "/api/downloadFileByName/" + name;
    } catch (IOException ex) {
      throw new IOFileUploadException("Could not store file " + fileName + ". Please try again!", ex);
    }
  }

  /**
   * Loads a file as a resource to return to the browser.
   * @param fileName
   * @return
   * @throws IOException
   */
  public Resource loadFileAsResource(String fileName) throws IOException {
    try {
      Path filePath = this.fileStorageLocation.resolve(fileName).normalize();
      Resource resource = new UrlResource(filePath.toUri());
      if (resource.exists()) {
        return resource;
      } else {
        throw new IOException("File not found " + fileName);
      }
    } catch (IOException ex) {
      throw new IOException("File not found " + fileName, ex);
    }
  }
}
