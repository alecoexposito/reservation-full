package ca.isucorp.reservation.service;

import ca.isucorp.reservation.model.ContactType;
import ca.isucorp.reservation.repository.ContactTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Service class to handle contact type's business related actions.
 */
@Service
public class ContactTypeService {

  private ContactTypeRepository contactTypeRepository;

  @Autowired
  public ContactTypeService(ContactTypeRepository contactTypeRepository) {
    this.contactTypeRepository = contactTypeRepository;
  }

  public List<ContactType> getAllContactTypes() {
    return this.contactTypeRepository.findAll();
  }
}
