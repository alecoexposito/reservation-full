package ca.isucorp.reservation.service;

import ca.isucorp.reservation.model.Contact;
import ca.isucorp.reservation.model.ContactType;
import ca.isucorp.reservation.model.Reservation;
import ca.isucorp.reservation.repository.ContactRepository;
import ca.isucorp.reservation.repository.ContactTypeRepository;
import ca.isucorp.reservation.repository.ReservationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Service class to handle contact's business related actions.
 */
@Service
public class ContactService {

  private ContactRepository contactRepository;
  private ContactTypeRepository contactTypeRepository;
  private ReservationRepository reservationRepository;

  @Autowired
  public ContactService(ReservationRepository reservationRepository, ContactRepository contactRepository, ContactTypeRepository contactTypeRepository) {
    this.contactRepository = contactRepository;
    this.contactTypeRepository = contactTypeRepository;
    this.reservationRepository = reservationRepository;
  }

  public Page<Contact> getAllContacts(Pageable pageable) {
    return this.contactRepository.findAll(pageable);
  }

  public Contact saveOrUpdateContact(Contact contact) {
    return this.contactRepository.save(contact);
  }

  public Contact getContactById(Integer id) {
    return this.contactRepository.findById(id).orElseThrow();
  }

  public void deleteContact(Integer id) {
//    Contact contact = this.contactRepository.findById(id).orElseThrow();
//    contact
    this.contactRepository.deleteById(id);
  }

  public ContactType getContactTypeById(Integer id) {
    return this.contactTypeRepository.findById(id).orElseThrow();
  }

  public Contact patchContact(Contact contact) {
    Contact contactToUpdate = this.getContactById(contact.getId());
    contactToUpdate.setName(contact.getName());
    contactToUpdate.setBirthDate(contact.getBirthDate());
    if (contact.getType() != null && !contactToUpdate.getType().getId().equals(contact.getType().getId())) {
      ContactType type = this.getContactTypeById(contact.getType().getId());
      contactToUpdate.setType(type);
    }
    return this.saveOrUpdateContact(contactToUpdate);
  }

    public List<Contact> searchByNameContaining(String term) {
      return this.contactRepository.findByNameContaining(term);
    }
}
