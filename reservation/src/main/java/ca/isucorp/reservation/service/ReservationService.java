package ca.isucorp.reservation.service;

import ca.isucorp.reservation.model.Reservation;
import ca.isucorp.reservation.repository.ReservationRepository;
import ca.isucorp.reservation.util.Util;
import org.apache.tomcat.util.http.fileupload.impl.IOFileUploadException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

/**
 * Service class for Reservation related business actions.
 */
@Service
public class ReservationService {

  private ReservationRepository reservationRepository;
  private final Path fileStorageLocation;
  private ContactService contactService;


  @Autowired
  public ReservationService(ReservationRepository reservationRepository, ContactService contactService) {
    this.reservationRepository = reservationRepository;
    this.fileStorageLocation = Paths.get("./uploads");
    this.contactService = contactService;
  }

  public Page<Reservation> getAllReservations(Pageable pageable) {
    return this.reservationRepository.findAll(pageable);
  }

  @Transactional
  public Reservation saveOrUpdateReservation(Reservation reservation) {
    this.contactService.saveOrUpdateContact(reservation.getContact());
    return this.reservationRepository.save(reservation);
  }

  public Reservation getReservationById(Integer id) {
    return this.reservationRepository.findById(id).orElseThrow();
  }

  public void deleteReservation(Integer id) {
    this.reservationRepository.deleteById(id);
  }

  public Reservation patchReservation(Reservation reservation) {
    return this.saveOrUpdateReservation(reservation);
  }

  public void addRatingToReservation(Integer id, Double evaluation) {
    this.reservationRepository.addRatingToReservation(id, evaluation);
  }
}
