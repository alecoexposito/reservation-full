package ca.isucorp.reservation.seeder;

import ca.isucorp.reservation.model.Contact;
import ca.isucorp.reservation.model.ContactType;
import ca.isucorp.reservation.model.Reservation;
import ca.isucorp.reservation.repository.ContactRepository;
import ca.isucorp.reservation.repository.ContactTypeRepository;
import ca.isucorp.reservation.repository.ReservationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * Class to seed information into the database.
 */
@Configuration
public class DatabaseSeeder {
  private static final Logger log = LoggerFactory.getLogger(DatabaseSeeder.class);

  /**
   * Method that saves some test information into the  database.
   * @param contactRepository
   * @param contactTypeRepository
   * @param reservationRepository
   * @return
   */
  @Bean
  @Transactional
  CommandLineRunner initDatabase(ContactRepository contactRepository, ContactTypeRepository contactTypeRepository, ReservationRepository reservationRepository) {
    return args -> {
      if (reservationRepository.count() == 0) {
        reservationRepository.deleteAll();
        contactRepository.deleteAll();
        contactTypeRepository.deleteAll();

      }
      if (contactTypeRepository.count() == 0) {
        contactTypeRepository.deleteAll();
        log.info("Loading Contact Types");
        ContactType contactType1 = contactTypeRepository.save(new ContactType("Contact Type 1"));
        ContactType contactType2 = contactTypeRepository.save(new ContactType("Contact Type 2"));
        ContactType contactType3 = contactTypeRepository.save(new ContactType("Contact Type 3"));
        log.info("Creating Contacts");
        Contact bilbo = contactRepository.save(new Contact("Bilbo Baggins", new Date(), contactType1, "2323232"));
        Contact frodo = contactRepository.save(new Contact("Frodo Baggins", new Date(), contactType2, "98989899"));
        Contact smeagol = contactRepository.save(new Contact("Smeagol", new Date(), contactType2, "11122121"));
        if (reservationRepository.count() == 0) {
          log.info("Creating reservations");
          reservationRepository.save(new Reservation("Description 1", bilbo, new Date()));
          reservationRepository.save(new Reservation("Description 2", bilbo, new Date()));
          reservationRepository.save(new Reservation("Description 3", smeagol, new Date()));
        }
      }
    };
  }
}
