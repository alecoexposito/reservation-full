package ca.isucorp.reservation.controller;

import ca.isucorp.reservation.service.FileStorageService;
import org.apache.tomcat.util.http.fileupload.impl.IOFileUploadException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
public class FileUploadController {

    @Autowired
    private FileStorageService fileStorageService;


//    @PostMapping(path = "/uploadFile")
//    public ResponseEntity uploadFile(@RequestParam("file") MultipartFile file) {
//        QFile qFile = fileStorageService.storeFile(file);
//
//        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
//                .path("/downloadFile/")
//                .path(qFile.getName())
//                .toUriString();
//
//        return ResponseEntity.ok(qFile);
//    }

  /**
   * Method handler for image upload
   * @param file
   * @return
   * @throws IOFileUploadException
   */
  @PostMapping(path = "/api/upload")
  public ResponseEntity<Map<String, String>> uploadFile(@RequestParam("file") MultipartFile file) throws IOFileUploadException {
    String url = null;
    Map<String, String> result = new HashMap<String, String>();
    try {
      url = this.fileStorageService.storeFile(file);
      result.put("location", url);
    } catch (IOFileUploadException e) {
      e.printStackTrace();
      throw e;
    }

    return ResponseEntity.ok(result);
  }

}
