package ca.isucorp.reservation.controller;

import ca.isucorp.reservation.model.Contact;
import ca.isucorp.reservation.model.ContactType;
import ca.isucorp.reservation.repository.ContactRepository;
import ca.isucorp.reservation.service.ContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.List;

@RestController
@RequestMapping("/api/contacts")
public class ContactController {

  private ContactService contactService;

  @Autowired
  public ContactController(ContactService contactService) {
    this.contactService = contactService;
  }

  @GetMapping("/")
  public ResponseEntity<Page<Contact>> getAll(Pageable pageable) {
    Page<Contact> contacts = this.contactService.getAllContacts(pageable);
    return ResponseEntity.ok(contacts);
  }

  @GetMapping("/{id}")
  public ResponseEntity<Contact> findById(@PathVariable Integer id) {
    Contact contact = this.contactService.getContactById(id);
    return ResponseEntity.ok(contact);
  }

  @PostMapping("/")
  public ResponseEntity<Contact> saveNew(@RequestBody Contact contact) {
    Contact savedContact = this.contactService.saveOrUpdateContact(contact);
    return new ResponseEntity<Contact>(contact, HttpStatus.CREATED);
  }

  @PutMapping("/")
  public ResponseEntity<Contact> update(@RequestBody Contact contact) {
    Contact savedContact = this.contactService.saveOrUpdateContact(contact);
    return new ResponseEntity<Contact>(contact, HttpStatus.OK);
  }

  @PatchMapping("/")
  public ResponseEntity<Contact> partialUpdate(@RequestBody Contact contact) {
    Contact updatedContact = this.contactService.patchContact(contact);
    return new ResponseEntity<Contact>(updatedContact, HttpStatus.ACCEPTED);
  }

  @DeleteMapping("/{id}")
  public ResponseEntity<Void> delete(@PathVariable Integer id) {
    try {
      this.contactService.deleteContact(id);
      return ResponseEntity.noContent().build();
    } catch (EmptyResultDataAccessException e) {
      return ResponseEntity.notFound().build();
    }
  }

  @GetMapping("/search/{term}")
  public ResponseEntity<List<Contact>> searchByNameContaining(@PathVariable String term) {
    List<Contact> contacts = this.contactService.searchByNameContaining(term);
    return ResponseEntity.ok(contacts);
  }
}
