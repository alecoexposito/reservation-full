package ca.isucorp.reservation.controller;

import ca.isucorp.reservation.model.Reservation;
import ca.isucorp.reservation.service.ReservationService;
import org.apache.commons.lang3.LocaleUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Locale;
import java.util.Map;


@RestController
@RequestMapping("/api/reservations")
public class ReservationController {

  private ReservationService reservationService;
  private MessageSource messageSource;

  @Autowired
  public ReservationController(MessageSource messageSource, ReservationService reservationService) {
    this.reservationService = reservationService;
    this.messageSource = messageSource;
  }

  @GetMapping("/")
  public ResponseEntity<Page<Reservation>> getAll(Pageable pageable) {
    Page<Reservation> reservations = this.reservationService.getAllReservations(pageable);
    return ResponseEntity.ok(reservations);
  }

  @GetMapping("/{id}")
  public ResponseEntity<Reservation> findById(@PathVariable Integer id) {
    Reservation reservation = this.reservationService.getReservationById(id);
    return ResponseEntity.ok(reservation);
  }

  @PostMapping("/")
  public ResponseEntity<Reservation> saveNew(@RequestBody Reservation reservation) {
    Reservation savedReservation = this.reservationService.saveOrUpdateReservation(reservation);
    return new ResponseEntity<Reservation>(reservation, HttpStatus.CREATED);
  }

  @PutMapping("/")
  public ResponseEntity<Reservation> update(@RequestBody Reservation reservation) {
    Reservation savedReservation = this.reservationService.saveOrUpdateReservation(reservation);
    return new ResponseEntity<Reservation>(reservation, HttpStatus.CREATED);
  }

  @PatchMapping("/")
  public ResponseEntity<Reservation> partialUpdate(@RequestBody Reservation reservation) {
    Reservation updatedReservation = this.reservationService.patchReservation(reservation);
    return new ResponseEntity<Reservation>(updatedReservation, HttpStatus.ACCEPTED);
  }

  @DeleteMapping("/{id}")
  public ResponseEntity<Void> delete(@PathVariable Integer id) {
    try {
      this.reservationService.deleteReservation(id);
      return ResponseEntity.noContent().build();
    } catch (EmptyResultDataAccessException e) {
      return ResponseEntity.notFound().build();
    }
  }

  @PostMapping("/addRating")
  public ResponseEntity<String> addRatingToReservation(@RequestHeader(name="Accept-Language", defaultValue = "en") String locale, @RequestBody Map<String, Double> params) throws Exception {
    Locale newLocale = null;
    try {
      newLocale = LocaleUtils.toLocale(locale);
    } catch (IllegalArgumentException e) {
      newLocale = Locale.US;
    }

    try {
      Integer id = params.get("id").intValue();
      Double evaluation = params.get("evaluation");
      this.reservationService.addRatingToReservation(id, evaluation);
      return ResponseEntity.ok(messageSource.getMessage("reservation.rating.success", null, newLocale));
    } catch (Exception e) {
      e.printStackTrace();
      throw new Exception(messageSource.getMessage("reservation.rating.error", null, newLocale));
    }
  }
}
