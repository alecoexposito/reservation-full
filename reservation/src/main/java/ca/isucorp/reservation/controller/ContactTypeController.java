package ca.isucorp.reservation.controller;

import ca.isucorp.reservation.model.ContactType;
import ca.isucorp.reservation.service.ContactTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/contactTypes")
public class ContactTypeController {

  private ContactTypeService contactTypeService;

  @Autowired
  public ContactTypeController(ContactTypeService contactTypeService) {
    this.contactTypeService = contactTypeService;
  }

  @GetMapping("/")
  public ResponseEntity<List<ContactType>> getAll() {
    List<ContactType> contactTypes = this.contactTypeService.getAllContactTypes();
    return ResponseEntity.ok(contactTypes);
  }
}
