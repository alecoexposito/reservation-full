package ca.isucorp.reservation.util;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.lang.Nullable;
import org.springframework.util.StringUtils;

import java.io.File;
import java.io.IOException;
import java.time.Instant;
import java.util.Date;

/**
 * Class with utility methods
 */
public class Util {

  /**
   * Method that returns a unique name for files that will be uploaded.
   * @param ext
   * @return
   */
  public static String getUniqueFileName(@Nullable String ext) {
    String name = RandomStringUtils.randomAlphanumeric(5) + String.valueOf(System.currentTimeMillis());
    if (!StringUtils.isEmpty(name))
      return name + ext;
    return name;
  }

  /**
   * Method to extract the extention from a file name.
   * @param filename
   * @return
   */
  public static String getExtFromFilename(String filename) {
    String ext = FilenameUtils.getExtension(filename);
    if (!StringUtils.isEmpty(ext))
      return "." + ext;
    return "";
  }

}
