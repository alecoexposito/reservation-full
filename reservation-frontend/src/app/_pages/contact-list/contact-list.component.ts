import { Component, OnInit } from '@angular/core';
import {ContactService} from '../../_services/contact.service';
import {Contact} from '../../_models/contact.model';
import {Page} from '../../_models/page';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import {ToastrService} from 'ngx-toastr';
import {UiService} from '../../_services/ui.service';

@Component({
  selector: 'app-contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.scss']
})
export class ContactListComponent implements OnInit {

  constructor(private toastr: ToastrService, public contactService: ContactService) { }

  ngOnInit(): void {
    this.contactService.loadContacts();
  }

  get contacts(): Contact[] {
    return this.contactService.contacts;
  }

  get page(): Page {
    return this.contactService.page;
  }

  deleteContact(id): void {
    Swal.fire({
      title: '¿Are you sure?',
      text: '¿Sure you want to delete this contact?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes I am',
      cancelButtonText: 'Cancel',
    }).then((result) => {
      if (result.value) {
        this.contactService.delete(id).subscribe(data => {
          this.contactService.loadContacts();
          this.toastr.success('Deleted', 'Contact has been deleted');
        });
      }
    });
  }

  onSort($event: any) {
    const sortStr = $event.sorts[0].prop + ',' + $event.sorts[0].dir;
    this.contactService.loadContacts({offset: 0}, sortStr);
  }

  isLowerThanMd(): boolean {
    return UiService.isLowerThanMd();
  }
}
