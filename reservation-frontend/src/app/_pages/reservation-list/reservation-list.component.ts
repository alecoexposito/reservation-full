import {Component, HostListener, OnInit} from '@angular/core';
import {ReservationService} from '../../_services/reservation.service';
import {Reservation} from '../../_models/reservation.model';
import {environment} from '../../../environments/environment.prod';
import {Page} from '../../_models/page';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import {ToastrService} from 'ngx-toastr';
import {CacheService} from '../../_services/cache.service';
import {UiService} from '../../_services/ui.service';


@Component({
  selector: 'app-reservation-list',
  templateUrl: './reservation-list.component.html',
  styleUrls: ['./reservation-list.component.scss']
})
export class ReservationListComponent implements OnInit {

  constructor(private cacheService: CacheService, private toastr: ToastrService, public reservationService: ReservationService) { }

  ngOnInit(): void {
    this.reservationService.loadReservations();
  }

  isLowerThanMd(): boolean {
    return UiService.isLowerThanMd();
  }

  get reservations(): Reservation[] {
    return this.reservationService.reservations;
  }

  getRowClass(): string | object {
    return 'custom-grey';
  }

  getImage(description): any {
    const tempDiv = document.createElement('div');
    tempDiv.innerHTML = description;
    const img = tempDiv.querySelector('img');
    if (img) {
      return img.src;
    } else {
      return '';
    }
  }

  get page(): Page {
    return this.reservationService.page;
  }

  addRate(id, evaluation: number): void {
    this.reservationService.addRating(id, evaluation).subscribe(data => {
      this.toastr.success('Reservation has been rated', 'Rated');
      this.reservationService.loadReservations({offset: this.page.pageNumber});
    });
  }

  isFavorite(id): boolean {
    return this.favorites.indexOf(id) !== -1;
  }

  get favorites(): number[] {
    return this.cacheService.favorites;
  }

  addFavorite(id: any): void {
    if (this.favorites.indexOf(id) === -1) {
      this.cacheService.addToFavorites(id);
    }
  }

  removeFromFavorites(id): void {
    if (this.favorites.indexOf(id) !== -1) {
      this.cacheService.removeFromFavorites(id);
    }
  }
}
