import { Component, OnInit } from '@angular/core';
import {UiService} from '../../_services/ui.service';

@Component({
  selector: 'app-create-contact',
  templateUrl: './create-contact.component.html',
  styleUrls: ['./create-contact.component.scss']
})
export class CreateContactComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  isLowerThanMd(): boolean {
    return UiService.isLowerThanMd();
  }
}
