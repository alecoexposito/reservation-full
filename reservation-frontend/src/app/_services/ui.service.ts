import { Injectable } from '@angular/core';

/**
 * Service class to return UI related information.
 */
@Injectable({
  providedIn: 'root'
})
export class UiService {

  constructor() { }

  static isLowerThanMd(): boolean {
    return window.innerWidth <= 992;
  }
}
