import { Injectable } from '@angular/core';
import {Observable, of} from 'rxjs';
import {HttpClient, HttpParams} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {environment} from '../../environments/environment';
import {Contact} from '../_models/contact.model';
import {Page} from '../_models/page';

/**
 * Service to store/get contact information.
 */
@Injectable({
  providedIn: 'root'
})
export class ContactService {
  contacts: Contact[] = [];
  page: Page = new Page();
  constructor(private http: HttpClient) {
    this.page.size = 10;
    this.page.pageNumber = 0;
  }
  search(term: string): any {
    if (term === '') {
      return of([]);
    }

    return this.http
      .get(environment.apiUrl + '/contacts/search/' + term).pipe(
        map((response: any) => response)
      );
  }

  loadContacts(pageInfo = {offset: 0}, sort = null): void {
    this.page.pageNumber = pageInfo.offset;
    if (sort !== null) {
      this.page.sort = sort;
    }
    this.http.get(environment.apiUrl + '/contacts/', {
      params: {
        page: this.page.pageNumber.toString(),
        size: this.page.size.toString(),
        sort: this.page.sort
      }
    }).subscribe((data: any) => {
      this.contacts = data.content;
      this.page.totalElements = data.totalElements;
      this.page.totalPages = data.totalPages;
      this.page.pageNumber = data.number;
    });
  }

  saveContact(value: any): any {
    return this.http.post(environment.apiUrl + '/contacts/', value);
  }

  getContactById(id: string): any {
    return this.http.get(environment.apiUrl + '/contacts/' + id);
  }

  updateContact(value: any): any {
    return this.http.put(environment.apiUrl + '/contacts/', value);
  }

  delete(id: any): any {
    return this.http.delete(environment.apiUrl + '/contacts/' + id);
  }
}
