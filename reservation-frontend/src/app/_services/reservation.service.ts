import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Reservation} from '../_models/reservation.model';
import {environment} from '../../environments/environment';
import {Page} from '../_models/page';

/**
 * Service class to store/get Reservation informations.
 */
@Injectable({
  providedIn: 'root'
})
export class ReservationService {

  reservations: Reservation[];
  page: Page = new Page();
  constructor(private http: HttpClient) {
    this.page.size = 10;
    this.page.pageNumber = 0;
  }
  loadReservations(pageInfo = {offset: 0}, sort = null): void {
    this.page.pageNumber = pageInfo.offset;
    if (sort !== null) {
      this.page.sort = sort;
    }
    this.http.get(environment.apiUrl + '/reservations/', {
      params: {
        page: this.page.pageNumber.toString(),
        size: this.page.size.toString(),
        sort: this.page.sort
      }
    }).subscribe((data: any) => {
      this.reservations = data.content;
      this.page.totalElements = data.totalElements;
      this.page.totalPages = data.totalPages;
      this.page.pageNumber = data.number;
    });
  }

  getImage(name): any {
    return this.http.get(environment.apiUrl + '/uploads/' + name);
  }

  saveReservation(body: Reservation): any {
    return this.http.post(environment.apiUrl + '/reservations/', body);
  }

  updateReservation(body: Reservation): any {
    return this.http.put(environment.apiUrl + '/reservations/', body);
  }

  getReservationById(id: string): any {
    return this.http.get(environment.apiUrl + '/reservations/' + id);
  }

  addRating(id: any, evaluation: number): any {
    return this.http.post(environment.apiUrl + '/reservations/addRating', {
      id,
      evaluation
    });
  }
}
