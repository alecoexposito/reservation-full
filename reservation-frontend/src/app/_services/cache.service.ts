import { Injectable } from '@angular/core';

/**
 * Service class to save and get information from the web storage.
 */
@Injectable({
  providedIn: 'root'
})
export class CacheService {

  favorites: number[] = [];
  constructor() {
    this.loadFavorites();
  }


  loadFavorites(): void {
    this.favorites = (localStorage.getItem('favorites') === null) ? [] : JSON.parse(localStorage.getItem('favorites'));
    console.log('favorites: ', this.favorites);
    if (this.favorites === null) {
      this.favorites = [];
    }
  }

  setFavorites(): void {
    localStorage.setItem('favorites', JSON.stringify(this.favorites));
  }

  addToFavorites(id: any): void {
    this.favorites.push(id);
    this.setFavorites();
  }

  removeFromFavorites(id: any): void {
    this.favorites.splice(this.favorites.indexOf(id), 1);
    this.setFavorites();
  }
}
