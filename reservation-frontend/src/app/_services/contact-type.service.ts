import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ContactType} from '../_models/contact-type.model';
import {environment} from '../../environments/environment';

/**
 * service to store/get ContactType information.
 */
@Injectable({
  providedIn: 'root'
})
export class ContactTypeService {

  contactTypes: ContactType[] = [];
  constructor(private http: HttpClient) {
    this.loadContactTypes();
  }

  loadContactTypes(): void {
    this.http.get(environment.apiUrl + '/contactTypes/').subscribe((data: any) => {
      this.contactTypes = data;
    });
  }
}
