import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ReservationListComponent} from './_pages/reservation-list/reservation-list.component';
import {CreateReservationComponent} from './_pages/create-reservation/create-reservation.component';
import {ContactListComponent} from './_pages/contact-list/contact-list.component';
import {CreateContactComponent} from './_pages/create-contact/create-contact.component';
import {EditContactComponent} from './_pages/edit-contact/edit-contact.component';
import {EditReservationComponent} from './_pages/edit-reservation/edit-reservation.component';

const routes: Routes = [{
  path: '',
  component: ReservationListComponent
}, {
  path: 'reservations/new',
  component: CreateReservationComponent
}, {
  path: 'contacts',
  component: ContactListComponent
}, {
  path: 'contacts/new',
  component: CreateContactComponent
}, {
  path: 'contacts/:id',
  component: EditContactComponent
}, {
  path: 'reservations/:id',
  component: EditReservationComponent
}];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
