import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './_components/header/header.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ReservationListComponent } from './_pages/reservation-list/reservation-list.component';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {HttpClientModule} from '@angular/common/http';
import { ReservationFormComponent } from './_components/reservation-form/reservation-form.component';
import { CreateReservationComponent } from './_pages/create-reservation/create-reservation.component';
import { ContactFormComponent } from './_components/contact-form/contact-form.component';
import { CreateContactComponent } from './_pages/create-contact/create-contact.component';
import { EditContactComponent } from './_pages/edit-contact/edit-contact.component';
import { ContactListComponent } from './_pages/contact-list/contact-list.component';
import {ReactiveFormsModule} from '@angular/forms';
import {ToastrModule} from 'ngx-toastr';
import {EditorModule} from '@tinymce/tinymce-angular';
import { EditReservationComponent } from './_pages/edit-reservation/edit-reservation.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ReservationListComponent,
    ReservationFormComponent,
    CreateReservationComponent,
    ContactFormComponent,
    CreateContactComponent,
    EditContactComponent,
    ContactListComponent,
    EditReservationComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    NgbModule,
    NgxDatatableModule,
    HttpClientModule,
    ReactiveFormsModule,
    ToastrModule.forRoot({
      progressBar: true
    }),
    EditorModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
