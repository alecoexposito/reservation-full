import {ContactType} from './contact-type.model';

export class Contact {
  id: number;
  name: string;
  phone: string;
  birthDate: Date;
  type: ContactType;
}
