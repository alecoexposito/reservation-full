import {Contact} from './contact.model';

export class Reservation {
  id: number;
  description: string;
  contact: Contact;
  reservationDate: Date;
  rating: number;
}
