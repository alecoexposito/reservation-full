import {AfterViewInit, Component, Input, OnInit, ViewChild} from '@angular/core';
import {Observable, of} from 'rxjs';
import {catchError, debounceTime, distinctUntilChanged, switchMap, tap} from 'rxjs/operators';
import {ContactService} from '../../_services/contact.service';
import {ReservationService} from '../../_services/reservation.service';
import {NgbDateStruct, NgbTimeStruct, NgbTypeaheadSelectItemEvent} from '@ng-bootstrap/ng-bootstrap';
import {ContactTypeService} from '../../_services/contact-type.service';
import {ContactType} from '../../_models/contact-type.model';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Contact} from '../../_models/contact.model';
import {Reservation} from '../../_models/reservation.model';
import {ActivatedRoute, Router} from '@angular/router';
import {environment} from '../../../environments/environment';
import {UiService} from '../../_services/ui.service';

@Component({
  selector: 'app-reservation-form',
  templateUrl: './reservation-form.component.html',
  styleUrls: ['./reservation-form.component.scss']
})
export class ReservationFormComponent implements OnInit {

  searching = false;
  searchFailed = false;
  reservationForm: FormGroup;
  @ViewChild('editor') editor;
  description = '';
  editorConfig: any = {
    height: 500,
    menubar: false,
    images_upload_url: environment.apiUrl + '/upload',
    plugins: [
      'advlist autolink lists link image charmap print preview anchor',
      'searchreplace visualblocks code fullscreen',
      'insertdatetime media table paste code help wordcount'
    ],
    toolbar: 'undo redo | formatselect | bold italic underline | alignleft aligncenter alignright |  bullist numlist | link image media emoticons | pagebreak preview | help'
  };
  @Input()
  private edit = false;


  constructor(private router: Router,
              private contactService: ContactService,
              private formBuilder: FormBuilder,
              private reservationService: ReservationService,
              private contactTypeService: ContactTypeService,
              private route: ActivatedRoute) {
  }

  isLowerThanMd(): boolean {
    return UiService.isLowerThanMd();
  }
  ngOnInit(): void {
    this.reservationForm = this.formBuilder.group({
      id: [''],
      name: ['', Validators.required],
      type: ['', Validators.required],
      phone: ['', Validators.required],
      birthDate: ['', Validators.required],
      description: ['', Validators.required],
      contactId: [''],
      reservationDate: ['', Validators.required],
      reservationTime: ['', Validators.required]
    });
    if (this.edit) {
      this.route.paramMap.subscribe(params =>  {
        const id = params.get('id');
        this.reservationService.getReservationById(id).subscribe(data => {
          this.f.id.setValue(id);
          this.loadContactFields(data.contact);
          const date = new Date(data.reservationDate);
          this.f.reservationDate.setValue({
            year: date.getFullYear(),
            month: date.getMonth() + 1,
            day: date.getDate()
          });
          this.f.reservationTime.setValue({
            hour: date.getHours(),
            minute: date.getMinutes()
          });

          this.f.description.setValue(data.description);
        });
      });
    }
  }

  loadContactFields(contact): void {
    this.f.contactId.setValue(contact.id);
    this.f.type.setValue(contact.type.id);
    this.f.phone.setValue(contact.phone);
    this.f.name.setValue(contact);
    const date = new Date(contact.birthDate);
    this.f.birthDate.setValue({
      year: date.getFullYear(),
      month: date.getMonth() + 1,
      day: date.getDate()
    });

  }

  formatter(result: any): any {
    return result.name;
  }

  search = (text$: Observable<string>) => {
    return text$.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      tap(() => this.searching = true),
      switchMap(term =>
        this.contactService.search(term).pipe(
          tap(() => this.searchFailed = false),
          catchError(() => {
            this.searchFailed = true;
            return of([]);
          }))
      ),
      tap(() => this.searching = false)
    );
  }

  selectContact($event: NgbTypeaheadSelectItemEvent<any>): any {
    const model = $event.item;
    this.loadContactFields(model);
  }

  get contactTypes(): ContactType[] {
    return this.contactTypeService.contactTypes;
  }

  onSubmit(): void {
    const body = new Reservation();
    body.description = this.reservationForm.value.description;
    const contact = new Contact();
    contact.id = this.reservationForm.value.contactId;
    contact.name = (typeof this.reservationForm.value.name === 'string')
      ? this.reservationForm.value.name : this.reservationForm.value.name.name;
    contact.phone = this.reservationForm.value.phone;
    const ngBirthDate = this.reservationForm.value.birthDate;
    contact.birthDate = this.getDateFromDateStructAndTimeStruct(ngBirthDate);
    const ngReservationDate = this.reservationForm.value.reservationDate;
    const ngReservationTime = this.reservationForm.value.reservationTime;
    body.reservationDate = this.getDateFromDateStructAndTimeStruct(ngReservationDate, ngReservationTime);
    contact.type = this.contactTypeService.contactTypes.find(ct => ct.id == this.reservationForm.value.type);
    body.contact = contact;
    let result;
    if (this.edit) {
      body.id = this.reservationForm.value.id;
      result = this.reservationService.updateReservation(body);
    }else {
      result = this.reservationService.saveReservation(body);
    }
    result.subscribe(() => {
      this.router.navigateByUrl('');
    });
  }

  get f(): any {
    return this.reservationForm.controls;
  }

  /**
   * Method that returns a javascript date object given the
   * NgBootstrap Date and Timer components values
   * @param dateStruct
   * @param timeStruct
   */
  getDateFromDateStructAndTimeStruct(dateStruct, timeStruct = null): any {
    const date = new Date(dateStruct.year, dateStruct.month, dateStruct.day);
    if (timeStruct !== null) {
      date.setHours(timeStruct.hour, timeStruct.minute);
    }
    return date;
  }
}
