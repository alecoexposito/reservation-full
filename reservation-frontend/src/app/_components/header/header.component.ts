import {Component, HostListener, OnInit} from '@angular/core';
import {UiService} from '../../_services/ui.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  isLowerThanMd(): boolean {
    return UiService.isLowerThanMd();
  }
}
