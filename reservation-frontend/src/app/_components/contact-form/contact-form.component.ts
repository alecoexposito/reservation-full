import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ContactService} from '../../_services/contact.service';
import {ContactTypeService} from '../../_services/contact-type.service';
import {Contact} from '../../_models/contact.model';
import {ActivatedRoute, Router} from '@angular/router';
import {UiService} from '../../_services/ui.service';

@Component({
  selector: 'app-contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.scss']
})
export class ContactFormComponent implements OnInit {

  contactForm: FormGroup;
  @Input()
  edit = false;
  contact: Contact;
  dateModel: any;
  minDate: {year: 1900, month: 1, day: 1};
  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private contactService: ContactService,
              private contactTypeService: ContactTypeService,
              private route: ActivatedRoute
  ) {
  }

  isLowerThanMd(): boolean {
    return UiService.isLowerThanMd();
  }

  ngOnInit(): void {
    this.contactForm = this.formBuilder.group({
      id: [''],
      name: ['', Validators.required],
      type: ['', Validators.required],
      phone: [''],
      birthDate: ['', Validators.required]
    });
    if (this.edit) {
      this.route.paramMap.subscribe(params =>  {
        const id = params.get('id');
        this.contactService.getContactById(id).subscribe(data => {
          this.contact = data;
          this.contactForm.patchValue(this.contact);
          this.contactForm.controls.type.setValue(this.contact.type.id);
          const date = new Date(this.contact.birthDate);
          this.dateModel = {
            year: date.getFullYear(),
            month: date.getMonth() + 1,
            day: date.getDate()
          };
          console.log('date model: ', this.dateModel);
        });
      });
    }
  }

  get f(): any {
    return this.contactForm.controls;
  }

  get contactTypes(): any {
    return this.contactTypeService.contactTypes;
  }

  onSubmit(): void {
    const body = new Contact();
    Object.assign(body, this.contactForm.value);
    const ngDate = this.contactForm.value.birthDate;
    body.birthDate = new Date(ngDate.year, ngDate.month, ngDate.day);
    body.type = this.contactTypeService.contactTypes.find(ct => ct.id == this.contactForm.value.type);
    let result;
    if (this.edit) {
      result = this.contactService.updateContact(body);
    }else {
      result = this.contactService.saveContact(body);
    }
    result.subscribe(() => {
      this.router.navigateByUrl('/contacts');
    });
  }
}
